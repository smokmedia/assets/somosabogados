SHELL=/bin/bash

PHONY: serve build

serve:
	cd site/ && hugo serve 

serve-toninas:
	cd site/ && hugo server --bind 0.0.0.0 --baseURL=http://192.168.0.176:1313 

build:
	rm -rf public && cd site/ && hugo && mv public/ .. && cd ..

cdk: build
	rm -rf cdk/site-contents && mv public/ cdk/site-contents && cdk deploy -c domain=smok.media -c subdomain=www

